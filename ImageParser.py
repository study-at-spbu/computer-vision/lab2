import cv2

from ImageParserCore import ImageParserCore
from Struct.ParseParameter import ParseParameter


class ImageParser:
    def parse(self, parse_parameter: ParseParameter):
        parser: ImageParserCore = ImageParserCore(parse_parameter)
        parser.parse()
        cv2.imshow('Result', parser.getResultImage())
        cv2.waitKey(0)
