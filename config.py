from pathlib import Path

from Struct.ParseParameter import ParseParameter

IMAGE_DIRECTORY: Path = Path("image")

PARSE_PARAMETERS = [
    ParseParameter(
        IMAGE_DIRECTORY / "logs-1.jpg",
        1.1,
        10,
        200,
        0.3,
        0,
        14,
        h=(2, 24)
    ),
    ParseParameter(
        IMAGE_DIRECTORY / "logs-2.jpg",
        1.5,
        9,
        200,
        0.6,
        0,
        0,
        h=(8, 18)
    ),
    ParseParameter(
        IMAGE_DIRECTORY / "pipes-4.jpg",
        1.5,
        15,
        160,
        0.9,
        0,
        0
    ),
    ParseParameter(
        IMAGE_DIRECTORY / "pipes-5.jpg",
        1.4,
        10,
        8,
        0.9,
        0,
        0,
        gaussian_kernel_size=5
    ),
    ParseParameter(
        IMAGE_DIRECTORY / "pipes-6.jpg",
        1.5,
        15,
        74,
        0.9,
        0,
        0,
        h=(0, 180),
        gaussian_kernel_size=5
    )
]
