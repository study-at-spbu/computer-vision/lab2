from ImageParser import ImageParser
from Struct.ParseParameter import ParseParameter
from config import PARSE_PARAMETERS

image_parser: ImageParser = ImageParser()

parse_parameter: ParseParameter
for parse_parameter in PARSE_PARAMETERS:
    image_parser.parse(parse_parameter)
