from dataclasses import dataclass
from pathlib import Path
from typing import Optional

from cv2.typing import Range


@dataclass
class ParseParameter:
    image_path: Path
    dp: float
    min_dist: float
    porog: float
    param2: float
    min_radius: int
    max_radius: int
    h: Optional[Range]
    gaussian_kernel_size: Optional[int]
