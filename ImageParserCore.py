import cv2
import numpy as np
import numpy.typing as npt
from cv2.typing import Range, MatLike

from Struct.ParseParameter import ParseParameter


class ImageParserCore:
    __image: npt.NDArray[np.uint8]
    __parse_parameter: ParseParameter
    __result_image: npt.NDArray[np.uint8]

    def __init__(self, parse_parameter: ParseParameter):
        self.__parse_parameter = parse_parameter
        self.__image = cv2.imdecode(np.fromfile(str(self.__parse_parameter.image_path.resolve()), dtype=np.uint8),
                                    cv2.IMREAD_COLOR)

    def parse(self) -> None:
        temp_image: npt.NDArray[np.uint8] = self.__image

        if self.__parse_parameter.h is not None:
            temp_image = self.__filterByColor(temp_image, self.__parse_parameter.h)

        temp_image = self.__convertToGray(temp_image)

        if self.__parse_parameter.gaussian_kernel_size is not None:
            temp_image = self.__getImageWithGaussianBlur(temp_image, self.__parse_parameter.gaussian_kernel_size)

        circles: npt.NDArray[np.float32] = self.__detectCircles(temp_image)
        self.__result_image = self.__getImageWithCircles(self.__image, circles)

    def getResultImage(self) -> npt.NDArray[np.uint8]:
        assert self.__result_image is not None, 'Method parse() Not Called'
        return self.__result_image

    def __filterByColor(self, image: npt.NDArray[np.uint8], h_range: Range) -> npt.NDArray[np.uint8]:
        img_hsv: npt.NDArray[np.uint8] = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        mask: npt.NDArray[bool] = np.zeros(shape=image.shape[0:2], dtype="bool")
        x: npt.NDArray[np.long] = np.where((img_hsv[:, :, 0] >= h_range[0]) & (img_hsv[:, :, 0] <= h_range[1]))
        mask[x] = 1
        masked_image = image.copy()
        masked_image[np.logical_not(mask)] = 0

        return masked_image

    def __convertToGray(self, image: npt.NDArray[np.uint8]) -> npt.NDArray[np.uint8]:
        return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    def __getImageWithGaussianBlur(self, image: npt.NDArray[np.uint8], gaussian_kernel_size: int) -> (
            npt.NDArray)[np.uint8]:
        return cv2.GaussianBlur(image, (gaussian_kernel_size, gaussian_kernel_size), 0)

    def __detectCircles(self, image: npt.NDArray[np.uint8]):
        return cv2.HoughCircles(image=image,
                                method=cv2.HOUGH_GRADIENT_ALT,
                                dp=self.__parse_parameter.dp,
                                minDist=self.__parse_parameter.min_dist,
                                param1=self.__parse_parameter.porog,
                                param2=self.__parse_parameter.param2,
                                minRadius=self.__parse_parameter.min_radius,
                                maxRadius=self.__parse_parameter.max_radius
                                )[0]

    def __getImageWithCircles(self, image: npt.NDArray[np.uint8], circles: MatLike) -> npt.NDArray[np.uint8]:
        temp_image: npt.NDArray[np.uint8] = image.copy()
        circles: npt.NDArray[np.uint16] = np.around(circles).astype(np.uint16)

        circle: npt.NDArray[np.uint16]
        for circle in circles:
            a, b, r = circle
            cv2.circle(temp_image, (a, b), r, (0, 255, 0), 2)
            cv2.circle(temp_image, (a, b), 2, (255, 0, 0), 3)

        return temp_image
